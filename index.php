<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 08.02.16
 * Time: 15:55
 */

require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/vendor/autoload.php';



$parser = new \nofuture17\parsers\TemplateParser([
    //'startUrl' => 'http://1-top.ru'
    'startUrl' => 'http://people.interfax.ru/samotlor/',
    'filterUrlRules' => [
//        '/' . preg_quote('/magento_52659/drill-machine.html', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/img/grandfather.jpg', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/img/archive-4.jpg', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/img/nyzhnevartovsk.jpg', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/img/grandson.jpg', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/img/497A0415.jpg', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/img/daughter.jpg', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/img/497A0373.jpg', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i2.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i3.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/gorod-1.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/img-timelaps.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/burovaya-23.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i1.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/gorod-1.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i2.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/img-timelaps.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i1.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i3.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/burovaya-23.mp4', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/gorod-1.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/img-timelaps.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i1.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i2.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/burovaya-23.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/gorod-1.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i3.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i1.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/img-timelaps.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i3.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/i2.webm', '/') . '$/',
        '/' . preg_quote('/assets/samotlor/video/burovaya-23.webm', '/') . '$/',
        '/' . preg_quote('/favicon.png', '/') . '$/',
        '/\S+\.[^(?:html)]+/'
    ]
]);

$addUrls = [
    '/assets/samotlor/img/grandfather.jpg',
    '/assets/samotlor/img/archive-4.jpg',
    '/assets/samotlor/img/nyzhnevartovsk.jpg',
    '/assets/samotlor/img/grandson.jpg',
    '/assets/samotlor/img/497A0415.jpg',
    '/assets/samotlor/img/daughter.jpg',
    '/assets/samotlor/img/497A0373.jpg',
    '/assets/samotlor/video/i2.mp4',
    '/assets/samotlor/video/i3.mp4',
    '/assets/samotlor/video/gorod-1.mp4',
    '/assets/samotlor/video/img-timelaps.mp4',
    '/assets/samotlor/video/burovaya-23.mp4',
    '/assets/samotlor/video/i1.mp4',
    '/assets/samotlor/video/gorod-1.mp4',
    '/assets/samotlor/video/i2.mp4',
    '/assets/samotlor/video/img-timelaps.mp4',
    '/assets/samotlor/video/i1.mp4',
    '/assets/samotlor/video/i3.mp4',
    '/assets/samotlor/video/burovaya-23.mp4',
    '/assets/samotlor/video/gorod-1.webm',
    '/assets/samotlor/video/img-timelaps.webm',
    '/assets/samotlor/video/i1.webm',
    '/assets/samotlor/video/i2.webm',
    '/assets/samotlor/video/burovaya-23.webm',
    '/assets/samotlor/video/gorod-1.webm',
    '/assets/samotlor/video/i3.webm',
    '/assets/samotlor/video/i1.webm',
    '/assets/samotlor/video/img-timelaps.webm',
    '/assets/samotlor/video/i3.webm',
    '/assets/samotlor/video/i2.webm',
    '/assets/samotlor/video/burovaya-23.webm',
    '/favicon.png',
];

foreach ($addUrls as $addUrl) {
    $parser->addToQueue($addUrl);
}

//$parser = new \nofuture17\parsers\ContentParser([
//    'startUrl' => 'http://1-top.ru/catalog',
//    'filterUrlRules' => [
//        '/' . preg_quote('/catalog', '/') . '$/',
//    ],
//    'selectors' => [
//        'names' => '.link_block',
//        'prices' => '.product_name'
//    ]
//]);

$parser->run();