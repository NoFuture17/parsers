<?php

/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 08.02.16
 * Time: 16:25
 */
namespace nofuture17\parsers;

class TemplateParser extends \nofuture17\parsers\Parser
{
    public $baseResultPath;
    public $taskResultPath;

    public static $htmlExts = [
        'html',
        'htm'
    ];

    public function init($config)
    {
        parent::init($config);

        $this->baseResultPath = __DIR__ . '/../result/';
        $this->taskResultPath = $this->baseResultPath . $this->getDomain() . '/';
    }

    public function proccessUrl($url)
    {
        $type = $this->getFileType($url);

        switch($type) {
            case 'html' :
                $result = $this->proccessHtml($url);
                break;
            case 'css' :
                $result = $this->proccessCss($url);
                break;
            case 'js' :
                $result = $this->proccessJs($url);
                break;
            default:
                $result = $this->proccessOtherType($url);
                break;
        }

        return $result;
    }

    protected function proccessHtml($url)
    {
        /*
        $response = $this->HTTPClient->request('GET', $url);

        if (!$this->checkResponseStatus($response->getStatusCode())) {
            return false;
        }

        $content = $response->getBody()->getContents();
        */
        $content = file_get_contents($this->getDomain(true) . $url);

        $this->getLinksFromHtmlContent($content);

        $levelDown = str_repeat('../', $this->currentUrlLevel);

        // Убираем имя домена
        $content = preg_replace(
            '/(href|src)="(?:(?:http|https)?\:?\/\/)'. preg_quote($this->getDomain(), '/') .'/',
            '$1="',
            $content
        );

        // Убираем последние слеши
        $content = preg_replace(
            '/(href|src)="([^\"]*)\/((?:\?|\#)[^\"]*)?"/',
            '$1="$2$3"',
            $content
        );

        // Добавляем index.html где нет разширения
        $content = preg_replace(
            '/(href|src)="((?:\.\.\/)*[^\s"\.\?\#]*)((?:\?|\#)[^\"]*)?"/',
            '$1="$2/index.html$3"',
            $content
        );

        // Удаляем первый сшешь, где путь абсолютный
        $content = preg_replace(
            '/(href|src)="\/(?!\/)/',
            '$1="',
            $content
        );

        // Делаем относительные пути
        $content = preg_replace(
            '/(href|src)="(?!(?:\.\.\/)|(?:(?:http|https)?\:?\/\/))/',
            '$1="' . $levelDown,
            $content
        );

        $filePath = $this->getFilePathByUrl($url);
        $this->saveFileFromContent($filePath, $content);

        return $filePath;
    }

    protected function getLinksFromHtmlContent($content)
    {
        parent::getLinksFromContent($content);

        if (!($content instanceof \Symfony\Component\DomCrawler\Crawler)) {
            $parser = $this->getDOMParser();
            $parser->addContent($content);
        }

        $links = $parser->filter('img');

        foreach ($links as $link) {
            $this->addToQueue($link->getAttribute('src'));
        }

        $links = $parser->filter('script');

        foreach ($links as $link) {
            $this->addToQueue($link->getAttribute('src'));
        }

        $links = $parser->filter('link');

        foreach ($links as $link) {
            $this->addToQueue($link->getAttribute('href'));
        }
    }

    protected function proccessCss($url)
    {
        $content = file_get_contents($this->getDomain(true) . $url);

        $this->getLinksFromCssContent($content);
        $filePath = $this->getFilePathByUrl($url);
        $this->saveFileFromContent($filePath, $content);

        return true;
    }

    protected function getLinksFromCssContent($content)
    {
        if (preg_match_all('/url\s?\(["\']*([^"\'()\s]+)["\']*\)/', $content, $matches)) {
            foreach ($matches[1] as $match) {
                $this->addToQueue($match);
            }
        }
    }

    protected function proccessJs($url)
    {
        $content = file_get_contents($this->getDomain(true) . $url);

        $this->getLinksFromJsContent($content);
        $filePath = $this->getFilePathByUrl($url);
        $this->saveFileFromContent($filePath, $content);

        return true;
    }

    protected function getLinksFromJsContent($content)
    {
        //url\s?\(["']*([^"'()\s]+)["']*\)
    }

    protected function proccessOtherType($url)
    {
        $content = file_get_contents($this->getDomain(true) . $url);

        $filePath = $this->getFilePathByUrl($url);
        $this->saveFileFromContent($filePath, $content);

        return true;
    }

    public function getFileType($fileName)
    {
        if ($fileName == '') {
            $fileName = $this->getStartUrl();
        }

        $ext = $this->getFileExt($fileName);
        $type = 'other';

        if (
            in_array($ext, ['png', 'jpg', 'jpeg'])
        ) {
            $type = 'image';
        } elseif ($ext == 'css') {
            $type = 'css';
        } elseif (in_array($ext, ['js', 'json'])) {
            $type = 'js';
        } elseif (empty($ext)  || in_array($ext, static::$htmlExts)) {
            $type = 'html';
        }

        return $type;
    }

    public function getFileExt($fileName)
    {
        $fileName = $this->clearQueryString($fileName);
        $fileName = $this->clearHashString($fileName);
        $ext = substr(strrchr($fileName, "."), 1);

        return empty($ext) ? false : $ext;
    }

    public function getFilePathByUrl($url)
    {
        $url = $this->clearQueryString($url);
        $url = $this->clearHashString($url);

        $fileType = $this->getFileType($url);
        $fileExt = $this->getFileExt($url);

        $fileName = $this->taskResultPath . $url;

        if ($fileType == 'html' && empty($fileExt)) {
            $fileName .= '/index.html';
        }

        return $this->clearDoubleSlashes($fileName);
    }

    public function getDirNameByFilePath($filePath)
    {
        return substr($filePath, 0, strrpos($filePath, '/'));
    }

    public function saveFileFromContent($filePath, $content)
    {
        $dirPath = $this->getDirNameByFilePath($filePath);
        @mkdir($dirPath, 0755, true);

        if (is_dir($dirPath)) {
            file_put_contents($filePath, $content);
            echo $filePath . PHP_EOL;
        }
    }
}