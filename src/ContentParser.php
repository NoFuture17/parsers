<?php
/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 02.03.16
 * Time: 8:48
 */

namespace nofuture17\parsers;


class ContentParser extends Parser
{
    /**
     * @var array Имя поля => селектор для извлечения данных из HTML
     */
    protected $selectors = [];

    /**
     * Parser constructor.
     * Принимает массив начальной конфигурации
     * ```
     *   [
     *       'startUrl' => 'http://example.com/', // Стартовый url
     *       'periodCallable' => function($parser) {...},
     *       'selectors' => [
     *           'title' => 'head > title',
     *           'content' => 'body > .contetnt'
     *       ],
     *       'maxLevel' => '3', // Максимальный уровень вложенности от стартового
     *       'normalizeUrlRules' => [
     *          function($url) {
     *              return preg_replace('/\?.+$/', '', $url);
     *          },
     *       ],
     *       'filterUrlRules' => [
     *          'exclude' => ['/.+(?:\.html|\.htm|\.js|\.css|\.png|\.jpeg)?$/'], // Исключающее правило
     *       ],
     *   ]
     * ```
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    protected function init($config)
    {
        parent::init($config);

        if (empty($config['selectors']) && !is_array($config['selectors'])) {
            throw new \Exception('Не указаны селекторы для контента');
        } else {
            $this->selectors = $config['selectors'];
        }
    }

    public function proccessUrl($url)
    {
        $content = file_get_contents($this->getDomain(true) . $url);

        $this->getLinksFromContent($content);

        $result = $this->getContent($content);

        return $result;
    }

    /**
     * Получает данные из HTML
     * @param $html
     * @return array
     */
    public function getContent($html) {
        $result = [];

        if (!($html instanceof \Symfony\Component\DomCrawler\Crawler)) {
            $parser = $this->getDOMParser();
            $parser->addContent($html);
        } else {
            $parser = $html;
        }

        foreach ($this->selectors as $key => $selector) {
            $elements = $parser->filter($selector);

            if (!empty($elements)) {
                foreach ($elements as $element) {
                    $result[$key][] = $element->ownerDocument->saveHTML($element);
                }
            }
        }

        return $result;
    }
}