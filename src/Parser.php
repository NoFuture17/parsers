<?php

/**
 * Created by PhpStorm.
 * User: nofuture17
 * Date: 08.02.16
 * Time: 16:25
 */
namespace nofuture17\parsers;

abstract class Parser
{
    public static $okStatuses = ['200'];

    public static $workStatuses = [
        'work'  => 1,
        'stop'  => 0,
        'wait'  => 2
    ];

    /**
     * @var int Работает ли парсер
     */
    public $workStatus = 1;

    /**
     * @var int Значение при кратности которому выполнять периодическую функцию
     */
    public $period = 20;

    /**
     * @var callable Функция, выполняемая пероидически.
     * Должна принимать объект парсера
     */
    protected $periodCallable;

    /**
     * @var int Максимальный уровень вложенности страниц
     */
    public $maxLevel = 3;

    /**
     * @var string Домен
     */
    protected $domain;

    /**
     * @var int Стартовый уровень вложенности
     */
    protected $startLevel = 0;

    /**
     * @var int Задержка между запросами
     */
    protected $requestTimeout = 2;

    /**
     * @var string Текущий url в обработке
     */
    protected $currentUrl;

    /**
     * @var int Текущий уровень вложенности url в обработке
     */
    protected $currentUrlLevel;

    /**
     * @var bool Разрешить редиректы для запроса
     */
    protected $allowRedirects = true;

    /**
     * @var string Протокол (http|https)
     */
    protected $scheme;

    /**
     * @var string Стартовый url;
     */
    protected $startUrl;

    /**
     * @var array Очередь ссылок на обработку
     */
    protected $queue = [];

    /**
     * @var int Счетчик обработаных url
     */
    protected $parsedCounter = 0;

    /**
     * @var array Список обработанных ссылок
     */
    protected $parsed = [];

    /**
     * @var array Список ссылок, которые обработать не удалось
     */
    protected $failed = [];

    /**
     * @var \Symfony\Component\DomCrawler\Crawler парсер html-докуметов
     */
    protected $DOMParser;

    /*
     * @var \GuzzleHttp\Client Клиент для http-запросов
     */
    protected $HTTPClient;

    /**
     * Результат работы парсера
     * @var array
     */
    protected $result = [];

    /**
     * Правила нормализации url
     * (возможны анонимные функции)
     * @var array
     */
    protected $normalizeUrlRules = [];

    /**
     * Правила фильтрации url
     * (возможны анонимные функции)
     * @var array
     */
    protected $filterUrlRules = [];

    /**
     * Parser constructor.
     * Принимает массив начальной конфигурации
     * ```
     *   [
     *       'startUrl' => 'http://example.com/', // Стартовый url
     *       'maxLevel' => '3', // Максимальный уровень вложенности от стартового
     *       'periodCallable' => function($parser) {...},
     *       'normalizeUrlRules' => [
     *          function($url) {
     *              return preg_replace('/\?.+$/', '', $url);
     *          },
     *       ],
     *       'filterUrlRules' => [
     *          'exclude' => ['/.+(?:\.html|\.htm|\.js|\.css|\.png|\.jpeg)?$/'], // Исключающее правило
     *       ],
     *   ]
     * ```
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->init($config);
    }

    /**
     * Функция запуска парсера
     */
    public function run()
    {
        while(
            !empty($this->queue)
            && $this->workStatus != self::$workStatuses['stop']
        ) {
            if (
                !$this->beforeIteration()
                || $this->workStatus == self::$workStatuses['wait']
            ) {
                continue;
            }

            $url = $this->getNextUrl();
            $this->addToResult($this->proccessUrl($url));
            $this->afterIteration();
        }

        return $this->getResult();
    }

    /**
     * Обработка страницы
     * @param string $url Текущая ссылка для обработки
     *  - Выполнить запрос
     *  - Сформировать дом
     *  - собираем все ссылки $this->getLinksFromContent()
     *  - Взять что нужно
     *  - вернуть результат
     */
    abstract public function proccessUrl($url);

    /**
     * Находит ссылки в $content и добавляет их в очередь на обработку
     * @param string|\Symfony\Component\DomCrawler\Crawler $content
     */
    public function getLinksFromContent($content)
    {
        if (!($content instanceof \Symfony\Component\DomCrawler\Crawler)) {
            $parser = $this->getDOMParser();
            $parser->addContent($content);
        } else {
            $parser = $content;
        }

        $links = $parser->filter('a');

        foreach ($links as $link) {
            $this->addToQueue($link->getAttribute('href'));
        }
    }

    /**
     * Проверить правильность кода статуса ответа
     * @param $statusCode
     * @return bool
     */
    public function checkResponseStatus($statusCode) {
        return in_array($statusCode, static::$okStatuses);
    }

    /**
     * Очищает строку url от параметров запроса (?aaa=aa)
     * @param string $url
     * @return string
     */
    public function clearQueryString($url)
    {
        return preg_replace('/\?.*/', '', $url);
    }

    /**
     * Очищает строку url от хеша запроса (#aaa)
     * @param string $url
     * @return string
     */
    public function clearHashString($url)
    {
        return preg_replace('/\#.*/', '', $url);
    }

    /**
     * Убирает двойные слеши из строки
     * @param $url
     * @return string
     */
    public function clearDoubleSlashes($url)
    {
        return preg_replace('/[\/]{2,}/', '/', $url);
    }

    /**
     * Приводит относительный путь к абсолютному(от корня)
     * @param $url string
     * @return string
     */
    public function urlStepUp($url)
    {
        if (preg_match_all('/(?:\.\.\/)/', $url, $matches)) {
            $count = count($matches[0]);
            $baseUrl = preg_replace('/(?:[^\s\/]+\/){0,' . $count . '}[^\s\/]*$/', '', $this->currentUrl);
            $baseUrl = rtrim($baseUrl, '/') . '/';
            $url = str_replace('../', '', $url);
            $url = ltrim($url, '/');
            $url = $baseUrl . $url;
        }

        return $url;
    }

    /**
     * @param string $url
     * @return string|bool Нормализованный $url
     */
    public function normalizeUrl($url)
    {
        $pattern = '/(?:\S*(?:\:\/\/)(?:www\.)?' . preg_quote($this->domain) . ')/';
        $url = trim($url);

        if (($url[0] != '/' && strstr($url, '../') === false) && !preg_match($pattern, $url)) {
            return false;
        }

        $url = preg_replace($pattern, '', $url);
        $url = $this->clearHashString($url);
        $url = '/' . trim($url, '/');

        foreach ($this->normalizeUrlRules as $rule) {
            $url = $this->applyNormalizeRule($url, $rule);
        }

        $url = $this->urlStepUp($url);

        return $url;
    }

    /**
     * Проверяет, можно ли добавить url в очередь на обработку
     * @param string $url
     * @return bool
     */
    public function filterUrl($url)
    {
        if (empty($url) || $url == '/' || preg_match('/^\#.*$/', $url)) {
            return false;
        }

        if (!empty($this->filterUrlRules['exclude'])) {
            $exclude = $this->filterUrlRules['exclude'];
            unset($this->filterUrlRules['exclude']);
            foreach ($exclude as $rule) {
                if (!$this->applyFilterRule($url, $rule)) {
                    return false;
                }
            }
        }

        foreach ($this->filterUrlRules as $rule) {
            if ($this->applyFilterRule($url, $rule)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Применяет правило фильтра для $url
     * @param string $url
     * @param string|callable|array $rule
     * @return bool Если успешно - true, иначе - false
     */
    public function applyFilterRule($url, $rule, $exclude = false)
    {
        if (is_string($rule)) {
            return preg_match($rule, $url);
        } elseif ($exclude) {
            return !preg_match($rule, $url);
        } elseif (is_callable($rule)) {
            return call_user_func($rule, $url);
        }

        return false;
    }

    /**
     * Применяет правило нормализации для url
     * @param string $url
     * @param string|callable $rule
     * @return string|bool Url по правилу нормализвации
     */
    public function applyNormalizeRule($url, $rule)
    {
        if (empty($url) || !is_callable($rule)) {
            return false;
        }

        return call_user_func($rule, $url);
    }

    public function getStartUrl($scheme = false)
    {
        if ($scheme) {
            $url = $this->getDomain($scheme) . $this->startUrl;
        } else {
            $url = $this->startUrl;
        }

        return $url;
    }

    public function getDomain($scheme = false)
    {
        $url = $this->domain;
        if ($scheme) {
            $url = $this->scheme . '://' . $url;
        }

        return $url;
    }

    public function getResult()
    {
        return $this->result;
    }

    /**
     * Удаляет данные из массива результатов или очищает его полностью
     * @param int|null $index Индекс в массиве результатов
     * @return void
     */
    public function clearResult($index = null) {
        if (is_numeric($index) && isset($this->result[$index])) {
            unset($this->result[$index]);
        } else {
            $this->result = [];
        }
    }

    protected function getNextUrl()
    {
        $this->currentUrl = array_shift($this->queue);
        $this->currentUrlLevel = $this->countUrlLevel($this->currentUrl);
        return $this->currentUrl;
    }

    /**
     * Функия выполняющаяся периодичестки
     * зависит от $parsedCounter, $periodCallable и $period
     * @return void
     */
    protected function periodFunction() {
        if (is_callable($this->periodCallable)) {
            call_user_func_array($this->periodCallable, ['parser' => $this]);
        }
    }

    /**
     * Вернёт объект DOM-парсера
     * @param bool $original (если true - вернёт оригинал, иначе - клон объекта)
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    public function getDOMParser($original = false)
    {
        return clone $this->DOMParser;
    }

    /**
     * Добавить ссылку в обработанные ($parsed)
     * @param string $url
     * @param bool $fail Если true, то в $failed
     */
    protected function addToParsed($url, $fail = false)
    {
        if (empty($url)) {
            return;
        }

        if ($fail) {
            $this->failed[] = $url;
        } else {
            $this->parsed[] = $url;
        }

        $this->parsedCounter++;
    }

    /**
     * Выполняется после обработкой url
     */
    protected function afterIteration()
    {
        $this->addToParsed($this->currentUrl);
        if (($this->parsedCounter % $this->period) == 0) {
            $this->periodFunction();
        }
    }

    /**
     * Выполняется перед обработкой url
     * @return bool false в случае если обработка данного url не требуется
     */
    protected function beforeIteration()
    {
        return true;
    }

    /**
     * Добавить ссылку в очередь
     * Происходит нормализация и фильтрация url
     * @param string $url
     */
    public function addToQueue($url)
    {
        $url = $this->normalizeUrl($url);

        if (
            $this->filterUrl($url)
            && !in_array($url, $this->queue)
            && !in_array($url, $this->failed)
            && !in_array($url, $this->parsed)
        ) {
            $this->queue[] = $url;
        }
    }

    /**
     * Добавляет результат обработки url в общий массив результатов ($this->result)
     * @param array $data
     */
    protected function addToResult($data)
    {
        if (!empty($data)) {
            $this->result[] = $data;
        }
    }

    /**
     * Проверить допустимость уровня вложенности ссылки
     * @param string $url
     * @return bool
     */
    protected function checkMaxLevel($url)
    {
        $url = $this->countUrlLevel($url);
        $diff = $url - $this->startLevel;

        return ($diff > $this->maxLevel) ? false : true;
    }

    /**
     * Вычисляет уровень вложенности url по слешам
     * @param string $url
     * @return int
     */
    public function countUrlLevel($url) {
        $level = 0;

        //'/(\/[^:\/]+)/'
        if (preg_match_all('/(\/[^:\/\.]+)(?=\/|$)/', $url, $matches)) {
            $level =  count($matches[1]);
        }

        //return ($level > 1) ? (int) ($level - 1) : 0;
        return  (int) $level;
    }

    /**
     * Устанавливает начальный url ($this->startUrl)
     * @param string $url
     * @return bool
     */
    protected function setStartUrl($url)
    {
        $url = rtrim($url);
        $url = rtrim($url, '/');

        if (empty($url)) {
            return false;
        }

        if (strstr($url, '://') === false) {
            $this->scheme = 'http';
        } elseif(strstr($url, 'http://') !== false) {
            $this->scheme = 'http';
        } elseif(strstr($url, 'https://') !== false) {
            $this->scheme = 'https';
        } else {
            $this->scheme = 'http';
        }

        $url = preg_replace('/(?:\S*(?:\:\/\/))/', '', $url);
        $this->domain = preg_replace('/^([^\s\/]+)[\S\s]*/', '$1', $url);

        $this->startUrl = str_replace($this->getDomain(), '', $url);
        $this->currentUrl = $this->startUrl;
        $this->queue[] = $this->startUrl;

        $this->startLevel = $this->countUrlLevel($this->startUrl);
        $this->currentUrlLevel = $this->startLevel;

        return true;
    }

    /**
     * Выполняет начальную конфигурацию
     * @param array $config
     */
    protected function init($config)
    {
        // Получаем стартовый урл
        if (
            empty($config['startUrl'])
            || !$this->setStartUrl($config['startUrl'])) {
            throw new \Exception('Не указан или указан не правильный стартовый url');
        }

        if (!empty($config['allowRedirects'])) {
            $this->allowRedirects = $config['allowRedirects'];
        }

        if (!empty($config['requestTimeout'])) {
            $this->requestTimeout = $config['requestTimeout'];
        }

        if (!empty($config['maxLevel'])) {
            $this->maxLevel = $config['maxLevel'];
        }

        if (!empty($config['period'])) {
            $this->period = $config['period'];
        }

        if (!empty($config['periodCallable'])) {
            $this->periodCallable = $config['periodCallable'];
        }

        if (!empty($config['normalizeUrlRules'])) {
            $this->normalizeUrlRules = array_merge(
                $this->normalizeUrlRules,
                $config['normalizeUrlRules']
            );
        }

        if (!empty($config['filterUrlRules'])) {
            $this->filterUrlRules = array_merge(
                $this->filterUrlRules,
                $config['filterUrlRules']
            );
        }

        $this->DOMParser = new \Symfony\Component\DomCrawler\Crawler();
        $this->HTTPClient = new \GuzzleHttp\Client([
            'base_uri' => $this->getDomain(true),
            'allow_redirects' => $this->allowRedirects,
            'timeout' => $this->requestTimeout,
        ]);
    }
}